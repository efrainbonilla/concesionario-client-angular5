import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../services/user.service';
import { Car } from '../../models/car';
import { CarService } from '../../services/car.service';

@Component({
  selector: 'app-car-edit',
  templateUrl: '../car-new/car-new.component.html',
  styleUrls: ['./car-edit.component.css'],
  providers: [ UserService, CarService ]
})

export class CarEditComponent implements OnInit {
  public page_title: string;
  public status_car: string;
  public message: string;
  public car: Car;
  public token: string;


  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _carService: CarService
  ) {

  }

  ngOnInit() {
    console.log('car-edit.component cargado correctamente');
    this.car = new Car(1, '', '', 1, '', null, null);
    this.token = this._userService.getToken();
    this.getCar();
  }

  onSubmit(form) {
    console.log(this.car);

    this._carService.update(this.token, this.car.id, this.car).subscribe(
      response => {
        console.log(response);
        if (response.status == 'success') {
          this.status_car = 'success';
          this._router.navigate(['car/show/', this.car.id]);
        } else {
          this.status_car = 'error';
        }
      }, error2 => {
        console.log(error2.error);
      }
    );
  }

  getCar() {
    this._route.params.subscribe(params => {
      if (params['id']) {
        let id = params['id'];
        this._carService.getCar(this.token, id).subscribe(
          response => {
            if (response.status == 'success') {
              this.car = response.car;
              this.page_title = 'Editar: ' + this.car.title;
            } else {
              this._router.navigate(['home']);
            }
          }, error2 => {
            console.log(error2.error);
          }
        );
      }
    });
  }
}
