import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';
import {isObject} from 'rxjs/util/isObject';
import {isString} from 'util';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  providers: [ UserService ]
})

export class LoginComponent implements OnInit {
  public title: string;
  public user: User;
  public status: string;
  public message: string;
  public token: string;
  public identity: string;

  constructor(
    private _userService: UserService,
    private _route: ActivatedRoute,
    private _router: Router
  ){
    this.title = 'Identificate';
    this.user = new User(1, 'ROLEUSER', '', '', '', '');
  }

  ngOnInit() {
    console.log('login.component ' + this.title + ' cargado correctamente!!');

    this.logout();
  }

  onSubmit(form) {
    console.log(this.user);

    this._userService.signup(this.user).subscribe(
      response => {

        if (isObject(response)){
          if (response.status == 'error') {

            this.status = 'error';
            this.message = response.message;
          }
        } else if (isString(response)) {

          //token
          this.token = response;
          localStorage.setItem('token', this.token);
          this.status = 'success';
          this._userService.signup(this.user, true).subscribe(
            resp => {
              this.identity = resp;
              localStorage.setItem('identity', JSON.stringify(this.identity));
              this._router.navigate(['home']);
            },
            error2 => {
              console.log(error2);
            }
          )
        }
        // usuario identificado

      },
      error => {
        this.status = 'error';
      }
    )
  }

  logout() {
    this._route.params.subscribe(params => {
      let logout = + params['sure'];
      if (params['sure']) {
        localStorage.removeItem('identity');
        localStorage.removeItem('token');

        this.identity = null;
        this.token = null;

        //redireccion
        this._router.navigate(['home']);
      }
    });
  }
}
