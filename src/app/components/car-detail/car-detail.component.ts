import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../services/user.service';
import { Car } from '../../models/car';
import { CarService } from '../../services/car.service';

@Component({
  selector: 'app-car-detail',
  templateUrl: './car-detail.component.html',
  styleUrls: ['./car-detail.component.css'],
  providers: [UserService, CarService]
})
export class CarDetailComponent implements OnInit {
  public title: string;
  public car: Car;
  public token: string;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _carService: CarService
  ) {

  }

  ngOnInit() {
    console.log('car-detail.component iniciado correctamente.');
    this.car = new Car(1, '', '', 1, '', null, null);
    this.token = this._userService.getToken();
    this.getCar();
  }

  getCar() {
    this._route.params.subscribe(params => {
      if (params['id']) {
        let id = params['id'];
        this._carService.getCar(this.token, id).subscribe(
          response => {
            if (response.status == 'success') {
              this.car = response.car;
            } else {
              this._router.navigate(['home']);
            }
          }, error2 => {
            console.log(error2.error);
          }
        );
      }
    });
  }

}
