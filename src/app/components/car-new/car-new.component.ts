import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../services/user.service';
import { Car } from '../../models/car';
import { CarService } from '../../services/car.service';
import {isObject} from 'rxjs/util/isObject';
import {forEach} from '@angular/router/src/utils/collection';
import {isArray} from 'rxjs/util/isArray';

@Component({
  selector: 'app-car-new',
  templateUrl: './car-new.component.html',
  styleUrls: ['./car-new.component.css'],
  providers: [ UserService, CarService ]
})

export class CarNewComponent implements OnInit {
  public page_title: string;
  public identity;
  public token;
  public car: Car;
  public status_car: string;
  public message: string;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _carService: CarService
  ) {
    this.page_title = 'Crear nuevo coche';
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();

  }

  ngOnInit() {
    if (this.identity == null) {
      return this._router.navigate(['login']);
    }

    this.car = new Car(1, '', '', 1, '', null, null);
  }

  onSubmit(form) {
    console.log(this.car);

    this._carService.create(this.token, this.car).subscribe(
      response => {
        console.log(response);
        this.car = response.car;

        if (isObject(response) && response.status != 'undefined') {
          if (response.status == 'success') {
            this.status_car = 'success';
            this.message = 'El coche se ha guardado correctamente';
            this._router.navigate(['home']);
          } else {
            this.status_car = 'error';
            this.message = 'Error, en el formulario';
          }
        } else {
          this.status_car = 'error';

          this.message = 'datos invalidos';
        }
      },error => {
        console.log(<any>error.error);
        let message = '';
        for ( let item in error.error) {
            message +='<p>' +item;
           if (isArray(error.error[item])) {
             message += '<ul>';
             for( let item2 in error.error[item]) {
               message += "<li>" + error.error[item][item2] + '</li>';
             }
             message += '</ul>';
           }
           message += '</p>';
        }

        this.status_car = 'error';

        this.message = message;
      }
    )
  }

}
